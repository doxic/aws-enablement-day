# AWS Enablement Day

## M169 - Fargate Workshop

Durch die verschiedenen Aufträge zum Thema Cloud, Container und Infrastructure as Code haben die Lernenden im Modul 169 die Möglichkeit, die Tools und Services kennenzulernen und anzuwenden. Für diesen Workshop starten wir zu einem sehr späten Zeitpunkt, bei dem die bekannten Dienste mit Hilfe von CI/CD-Pipelines zu einem grossen Ganzen zusammenkommen.

Damit Sie mit möglichst wenig Aufwand am gleichen Punkt ankommen und die Übung selbstständig durchführen können, stellen wir Ihnen zum einen eine vorbereitete Umgebung mit den nötigen Tools zur Verfügung. Andererseits haben Sie Zugriff auf das M169 Gitlab-Repository mit einem weiteren Hilfsmittel für die unterrichtenden Lehrpersonen: Terraform - ein Infrastructure Automation Tool.

Die Datei [main.tf](https://gitlab.com/bbwin/m169-aws-fargate/-/blob/main/main.tf?ref_type=heads) enthält die Instruktionen, um die komplette AWS-Infrastruktur der Lernenden zu reproduzieren. Es ermöglicht der Lehrperson, in verschiedenen Klassen und zu unterschiedlichen Zeitpunkten schnell und unkompliziert eine Vorzeige-Infrastruktur zu erhalten.

## Virtuelle Workshop-Umgebung

Unter der Webseite https://${URL} haben Sie Zugriff auf eine persönliche Webversion mit VS Code inklusive einem Web-Terminal. Benutzen Sie die Zugriffsdaten unter [Login](#Login). Die Umgebung hat alle für den Workshop notwendigen Tools bereits vorinstalliert. Die Aufgabe "30.4 CI/CD-Pipeline" können Sie nach belieben wie die Lernenden auf Ihrer lokalen Arbeitsmaschine ausführen oder alternativ ebenfalls in der bereitgestellten Umgebung

## Login

**URL**: ${URL}

**Passwort**: `${PASSWORD}`

## Vorbereitung Zugangsdaten zu AWS Learner Lab

- Melden Sie sich mit Ihrem Passwort bei **VS Code** an. Im Startbildschirm haben Sie die Möglichkeit, VS Code Ihren Bedürfnissen anzupassen.

- Melden Sie sich in einem weiteren Browser-Tab in Ihrem **AWS Lerner Lab** an (https://awsacademy.instructure.com/)

- Starten Sie zuerst Ihr Learner Lab mittels "**▸ Start Lab**"

- Sobald neben "**AWS** 🟢" ein grüner Kreis erscheint ist Ihr Lab gestartet und Sie können über "ℹ **AWS Details**" Ihre Zugangsdaten für die AWS CLI anzeigen lassen. **ACHTUNG**: Diese Zugangsdaten erneuern sich nach jeder Deaktivierung des Learner Lab. Wiederholen Sie in diesem Fall die Schritte um die neuen Zugangsdaten zu erhalten.

  ![Selection_001](/home/dominic/workspace/m169-aws-fargate/assets/learnerlab.png)

- Kopieren Sie nun den kompletten Text in die Zwischenablage
  ![Selection_002](/home/dominic/workspace/aws-enablement-day/assets/awscli.png)

- Öffnen Sie die Datei `~/.aws/credentials` in VS Code und fügen Sie den Inhalt der Zwischenablage ein.
  ![Selection_002](/home/dominic/workspace/aws-enablement-day/assets/credentials.png)

- Ihre Umgebung ist nun vorbereitet kann sich mit Terraform in Ihr Learner Lab verbinden.

## Terraform

- Über das Menü "Terminal" > "New terminal" oder mit dem Tastaturkürzel Ctrl + Shift + C öffnen Sie ein Terminal.
  ![Selection_002](/home/dominic/workspace/aws-enablement-day/assets/terminal.png)

- In Ihrem Terminal wechseln Sie nun ins Verzweichnis `m169-aws-fargate`
  ```sh
  $ cd m169-aws-fargate
  ```

- Damit Terraform mit den AWS API kommunizieren kann, muss initial der AWS Provider installiert werden. Dies wird mittels dem Befehl `terraform init` ausgeführt.
  ```sh
  $ terraform init
  Initializing the backend...
  Initializing provider plugins...
  - Reusing previous version of hashicorp/aws from the dependency lock file
  - Installed hashicorp/aws v4.61.0 (signed by HashiCorp)
  ...
  Terraform has been successfully initialized!
  ...
  ```

- Terraform durchläuft jeweils drei Phasen: `plan` für eine vorgängige Überprüfung, `apply` für die produktive Umsetzung und `destroy` um die Infrastruktur wieder zu entfernen. Testen Sie vorab mit `terraform plan`
  ```sh
  $ terraform plan
  data.aws_availability_zones.available: Reading...
  ...
  Terraform used the selected providers to generate the following execution plan. Resource actions are indicated
  with the following symbols:
    + create
  
  Terraform will perform the following actions:
  
    # aws_default_subnet.primary will be created
  ...
  Plan: 9 to add, 0 to change, 0 to destroy.
  ```

  Beachten Sie die Zeile `Plan: 9 to add`. Terraform wird somit im weitern Schritt neun neue Ressourcen in Ihrem AWS Learner Lab Umgebung erstellen.

- Erstellen Sie diese nun mit `terraform apply`. Die Ausgabe erinnert wiederum an die `plan`  Phase. Bestätigen Sie "Do you want to perform these actions?" mit "**yes**".
  ```sh
  $ terraform apply
  ...
  Plan: 9 to add, 0 to change, 0 to destroy.
  
  Do you want to perform these actions?
    Terraform will perform the actions described above.
    Only 'yes' will be accepted to approve.
  
    Enter a value: yes
  ...
  Apply complete! Resources: 9 added, 0 changed, 0 destroyed.
  ```

- Ihre Infrastruktur ist nun in der **AWS Management Console** einsehbar. Öffnen Sie diese mit einem Klick auf **"AWS** 🟢" in Ihrem Learner Lab.
  **ACHTUNG**: Im Learner Lab haben Sie die Regionen (Datencenter) `us-east-1` sowie `us-west-2` zur Verfügung. Terraform wählt standardmässig `us-west-2` als Region. Wechseln Sie oben Rechts von `N. Virginia` auf `Oregon` um die Services zu sehen.

- Die wichtigen Services finden Sie unter:

  - ![img](/home/dominic/workspace/aws-enablement-day/assets/ecr.svg) Elastic Container Registry
  - ![img](/home/dominic/workspace/aws-enablement-day/assets/ecs.svg) Elastic Container Services

- Falls Sie die Namen der erstellten Objekte verändern möchten, können Sie diese in der Datei `terraform.tfvars` anpassen und die Schritte `terraform plan` und `terraform apply` wiederholen.



Nun viel Erfolg bei der Aufgabe "30.4 CI/CD-Pipeline" unter https://bbwin.gitlab.io/m169-aws-fargate/iac/cicd/

