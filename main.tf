terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.14.0"
    }
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.35.2"
    }
    ansible = {
      source  = "ansible/ansible"
      version = "1.1.0"
    }
    htpasswd = {
      source  = "loafoe/htpasswd"
      version = "1.0.4"
    }
  }
  backend "http" {}
}

provider "ansible" {}
provider "cloudflare" {}
provider "hcloud" {}
provider "htpasswd" {}

data "hcloud_image" "jammy" {
  name = "ubuntu-22.04"
}

data "hcloud_ssh_key" "main" {
  name = "hetzner-doxic.id_rsa.pub"
}

resource "hcloud_server" "instance" {
  count       = 1
  name        = "instance-${count.index + 1}"
  image       = data.hcloud_image.jammy.id
  server_type = "cx21"  # 2vCPU 5GB"
  public_net {
    ipv4_enabled = true
  }

  ssh_keys = [data.hcloud_ssh_key.main.id]

  labels = {
    "environment" : "workshop"
  }
  lifecycle {
    ignore_changes = [image]
  }
}

resource "cloudflare_record" "instance" {
  count = length(hcloud_server.instance)

  zone_id = data.cloudflare_zone.bbwin.id
  type    = "A"
  name    = hcloud_server.instance[count.index].name
  value   = hcloud_server.instance[count.index].ipv4_address
}

data "cloudflare_zone" "bbwin" {
  name = "bbwin.ch"
}

resource "ansible_host" "instance" {
  count = length(hcloud_server.instance)

  name   = hcloud_server.instance[count.index].name
  groups = ["workshop"]


  variables = {
    ansible_host     = hcloud_server.instance[count.index].ipv4_address
    hostname         = cloudflare_record.instance[count.index].hostname
    student_password = random_password.password[count.index].result
    student_hash     = htpasswd_password.hash[count.index].sha512
  }
}

resource "ansible_group" "workshop" {
  name = "workshop"
  variables = {
    ansible_user = "root"
    # ansible_ssh_private_key_file = "vockey"
  }
}

resource "random_password" "password" {
  count   = length(hcloud_server.instance)
  length  = 8
  upper   = false
  special = false
}

resource "htpasswd_password" "hash" {
  count    = length(hcloud_server.instance)
  password = random_password.password[count.index].result
}

resource "local_file" "document" {
  count = length(cloudflare_record.instance)
  content = templatefile("templates/template.md", {
    URL      = cloudflare_record.instance[count.index].hostname
    PASSWORD = random_password.password[count.index].result
  })
  filename        = "${path.module}/public/${ansible_host.instance[count.index].name}.md"
  file_permission = "0644"
}
